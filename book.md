# Can we do better ?

## Preamble

Can we do better? I sincerely believe so. What you are about to read is a compilation of various topics, ideas and ponderings, that occupied my mind over the preceding years. They are in constant flux and undergo change as they are confronted with both reality, experiences of other people and constructive criticism.
Many of the ideas are not supported by indisputable facts, build upon assumptions or rough estimations done subjectively by me, so look at them from this perspective. Same as economics, this is opinionology, a book of opinions and ideas and not measurements, relations and formulas.
Can a book be written with some principles of open source while still retaining a semblance of authorship? I also believe so. I am open to feedback, criticisms and transformative works. While I might not be the one who gets to the ‘good enough solution’, I hope this book can act as inspiration for some that will.

## Index

* feel free to jump around

## History and religion

### Death and fear of it

Since we are born and come into existence, we begin the process of dying and ceasing to exist again. It is inevitable and can come suddenly, when we least expect it. It begets the ultimate question to which world’s religions provide an answer, with a small caveat – no proof.
The fear that comes from not knowing for sure what happens after finds comfortable band-aid in blindly believing preachers with conviction. Fear of death is also an instinct that keeps us moving forward, to cling to life and cherish life for as long as possible. Innate will of life to keep going on, despite apparent lack of the ultimate answer to the question ‘why?’ is fascinating.
What is in my opinion even more fascinating is the dependence of life on death. Living organisms upon death are transformed through various bacteria back to the building blocks and transform into another life. Our rotting corpses feed bacteria which produce nutrients for fungi, plants, which in turn upon death in some other living organism’s stomach provide them with little bit of time to live in their form.
This spark of life, ephemeral promise of potential, rides like a surfer on the wake of time, tirelessly trying out all possible venues, abandoning them in death so that it can recombine in a different form and keep hammering on.
In face of this certainty I feel like we should not dread death. In the scope of all things, we are both truly insignificant, but as a part of the whole movement of life, inevitable.
Don’t blindly follow. Be open to inputs, work with them, produce outputs, learn from feedback. Look around, listen. Enjoy life and let others enjoy it too. Breathe in, breathe out. Repeat.

### Free will, freedom and responsibility

There are two schools of thought that thread through our history and society.
One is the idea, that we have a free will. Ability to choose anything between infinite number of possibilities, some options or just yes and no. Constantly. That we exert any degree of control over our decisions. That we have innate freedom and thus resulting responsibility for those choices.
Other idea is, that everything is preordained – there is so called fate. Inputs plus transformation equals output. This idea is the basis of some of the religions teachings, that everything is the will of some divine power that controls everything and thus we are unable to do anything about it but submit to it.
There is a distinct possibility that neither of those is right. What if everything is a combination of energy, instructions how it behaves and an element of chaos during all quantum based probabilistic outputs. Latest discoveries in the fields of quantum physics seem to me to be pointing more and more towards this ‘third’ option. We are not in control but neither is anyone or anything else. Everything is just a constant choosing of one option from infinity in all places at the same time.
Fortunately enough in most of the justice systems around the world, the law recognizes different levels of responsibility for actions one takes, based on various factors that can be outside of control of the individual. This can be seen as a more or less successful attempt to blend both of those approaches into a scale.
Justice is not the only field those two conflicting ideas affect. Different perspective of individual freedom and responsibility directly ties into the currently viewed (and skewed) political leanings of left and right (right focusing (and believing in) on the individual freedom and responsibility underestimating the obvious interconnectedness of everything, where as left focuses on the interconnections and underestimates the individual responsibilities and control over individual’s decissions). More in chapter politics.

### Specialization

Since the history of human civilization our survival was determined by simple limiting factors. Drinkable water, food, shelter from elements, survive against other living creatures. This meant learning a lot of do’s and dont’s by trial and (usually) fatal error.
Drinking stale water with dead animals in it – don’t. Touching burning things – don’t. Some plants, fruits, nuts, worms, bugs, bird eggs, mushrooms – do. Some animals if we’re able to kill them – do. Eat rotten, moldy, poisoned or otherwise decomposing food – don’t (mostly).
This all applies to both individuals and groups of people. With increasing number of individuals living in (and thus sharing) the same area the complexity of those interactions increases. Suddenly you don’t only deal with your own survival having to handle every aspect of it, but you are both potentially endangered or helped by other individuals.
One of those lessons is: welcome everyone without making sure they won’t stab you in the back once you turn it to them – don’t. On the other hand helping those less fortunate so that when they’re better off, they can help you should you get into trouble.
This combination of ruthless competition with compassion is what was, is and probably will be part of humanity. Too much of one means the overall survival and well being is endangered.
Another emergent property of grouping of individuals is specialization. If one individual is really good hunter and other is really apt forager, why force the first one into foraging and the latter one into hunting? Both are better off if they specialize and then share or trade. More on sharing vs trading in the economy section.
The more individuals there are grouped in one area, if they have enough resources, the more specialized they can become and new occupations emerge. Instead of just foragers and hunters, we can have shamans (precursor before doctors and preachers), warriors (precursor before police and army), chieftain (respected leader as a precursor before government and judge) and so on. Then with every new disruptive technology a plethora of occupations appear. Miners, builders, loggers, tanners, blacksmiths, farmers, weavers, teachers and the list goes on.
Resume: Specialization is a natural process stemming from combination of multiple people sharing resources and new technologies
Depending on the overall availability of resources and the number of individuals in the group, the overall potential specialization depth is limited. If you have a small tribe living in the arctic, they won’t have much use for social media personalities, motivational speakers, beauty salons, movie producers, vault pole jumping trainers etc.

### Equality

Individuals differ. This is due to different genetic code and different external factors when they were born. If you are born to an African tribe before a couple hundred year, you would have zero chance of driving a Porsche or flying on a plane or having plenty of food and access to education. If you are born to a king in the middle of Europe though, your starting point will be a lot different.
One can’t affect what the starting point for him or her will be, but gradually we, as a society, come to value at least trying to bring everyone to the level, that once they will become legally responsible for their own actions the difference between contemporaries will be as low as possible (by bringing the less fortunate up, not by trying to achieve the equality by bringing the more fortunate down to the level of the rest). This is called equality of opportunity.
On the other hand, some people would like to continue this process later into life and not necessarily by just pulling the bottom up, but also by pushing the top down so the overall differences become smaller. This is called equality of outcome.
When people talk about equality, they often mean the latter. Make sure you ask them which one they mean first, because sometimes it also means ignoring the inputs and trying to force only the outputs to be the same. (equal pay for unequal (work)value put in)

### Inheritance

One of the factors affecting the overall well being of a group of people is what happens with the scarce resources (which can even be an artificially scarce resource by just means of force – money supply controlled by central bank, enforce by government through law) when some people either hoard them (reducing the supply available for exchanged between people) or pass them to the next generation through inheritance.
This basically means that in every generation, the starting conditions can be (and usually are) wildly different. Societal system should be able to handle this innate inequality of starting point and try to get everyone up to a certain speed before they become responsible for themselves. This is usually achieved through education which becomes the more valuable the more information is contained in all work and goods (more in chapter later).
Some ideologies tried to solve this issue by forgoing individual property rights and thus inequality through inheritance was in theory impossible. Powerful individuals in those societies found ways to circumvent this limit by various needs (through nepotism or cult of personality while still alive, basically turning into dynasties or oligarchies). (see north korea, many post-soviet republics). This defacto inheritance of power is even more common in capitalism based societies, though more or less counteracted by both anti-monopoly laws, and the fact that subsequent descendant generations of powerful individuals don’t have to posses the same qualities, or be given the same external conditions to retain this power or wealth.
On the other hands some dynasties managed to perfected this passing of power and wealth into complex systems and are known as ‘the old money’, usually exercising their power behind the scenes, veiled by the front of politicians beholden to them through need for money to stay in their slice of power. This relation is often hidden and diluted through complex web of contractual relations between large multinational corporations. In short, there are no omnipotent central illuminati but there are certainly powerful individuals and families pulling on many strings controlling what we perceive as politics.

### Sins of fathers

Symbolic ‘passing of the torch’ does not only affect the power and wealth but also negative factors. One of them is debt, which can usually be eventually overcome by diluting over time, or forgiving some. Much more impactful and insidious are so called ‘sins of fathers’.
This is anything ranging from harm caused by the proverbial father to someone, who will then blame not only the perpetrator but a group of people associated with him. The larger the scale and impact of the harm done, the broader the blame usually is. This blame can fester and turn into hate, which in turn can turn into more harm, starting the ‘spiral of violence’.
Clear example of this is the conflict between Israel and Palestine. Nobody can exactly pinpoint to the first act of an individual who started this whole spiral because the conflict lasts for so long and the ‘sins of fathers’ just keep being inflicted from both sides on each other.
From historical perspective conflicts like this can either dissipate on their own by people being simply tired of it and giving up on their former objective, or end up in a war which usually kills so many individuals on one side that they give up and become subjugated. Both of those processes are unfortunately usually very long (sometimes one war was not ‘purge’ the problem away enough and repeats with more harm being inflicted)
 From the recent memory there are not many examples of such a conflict ending abruptly besides the atrocities in Rwanda between the Hutu and Tootsie.
Unfortunately this instead feed to the so called cycle of war. War with all the atrocities, killing and general harm ends. People are relieved, catharsis purifies them and they appreciate absence of war – peace. Over time, people remembering those atrocities start dying from old age, memories become distant and dissatisfactions become grow again. Dissatisfactions (more in dissatisfaction vector below) get amplified by polarization, heralds of hate, ingroup and outgroup mentality and factions emerge. Factions blame their problems on the outgroup. Factions group up and tighten by emptying the space that used to bridge those differences and people prepare for another conflict. Conflict can then turn into war. Cycle repeats.
In order to lighten things a bit, usually this cycle can be slowed down or reversed a couple steps by sufficient effort of people aware of the cycle. Perseverance of such people then determines how long a peace lasts.

## Economy

Economy is a discipline that aims to study people’s behaviour, relations and interactions that have anything to do with how they’re off in terms of resources. It claims it’s a scientific discipline, but I digress. It’s a discipline of opinions dressed in scientific clothing and methodologies.

### Money 101

Money money money. Not just a song from ABBA. Money is another naturally emergent concept stemming from specialization and grouping of people.
Money is a sum of functions in a form of a tool, that is accepted by enough people, making it comfortable to use by most of them, to fulfill those functions.
To go back to the beginning, the hunter-gatherer society, the exchanges between people were always in a form of barter – you have something, I offer you something for it in exchanged. If we both value what we get from the exchange more than what we’ve given in, we’re both better off. You have meat from hunt that will spoil if not all eaten now, I just gathered some nuts that can last longer than the meat but would like to save some for when I actually need them. You have some nuts so you have later when the meat won’t be edible, and I’ll feed myself tonight with something else than these nuts, so I can also have them for later.
This form of exchanged was sufficient for a while but it has several limitations, which caused people to look for other means of facilitating this exchange.
Those limitations are: who exactly wants the item you have for trade, how much of it, when, what is asked in exchange, how long will your offer be even suitable for exchange etc, how abundant is the item you’re offering. You can clearly see that meat and nuts are not the best items to fulfill all those criteria.
Over time different media of exchange came about – for example flint – that was used to make many of the tools like arrowheads, spearheads, knives, axes, primitive needles. Everyone needed them, you could have larger and smaller ones,  they did not spoil, there were not totally abundant and the skill to make usable tools from them was not totally common.
You can see that even flint has some limitations and was unsurprisingly phased out by metals -  they are pliable, you can’t easily destroy them (you can break them, melt them and have coins again, can split them to very small quantities, can form them into many more (and complex) shapes than flint).
Eventually from all the metals we discovered at the time, rare metals became the dominant means of trade. Rarity and aesthetics meant that possession of those metals was a stature of power. The more of them you had, the more power you had. They lasted pretty much forever, could be transported (and also stolen at any time – you have to steal food only when it’s available and edible), did hold the value, could be divided (so could spend only a portion of it at a time).
Part of the value of money is (which Is something not appreciated enough) directly related to the state of specialization of the society and available resources. You don’t have much use for bitcoins in the middle of the Sahara desert. Where as clean drinkable water can be worth it’s weight in gold. Same goes for being stranded on an island or in the middle of nowhere in a small tribe of natives. Not much use for cash, bonds or credit cards there, either.

### Interest and debt based economy

Ever since the adoption of any form of money, some people had more of it than others. Also, more often then not, the ones with it did not use it all the time. Same as with any other tool that you can borrow from someone, you can do some work with the tool that allows you to make something worth more than something you pay in exchange for borrowing that tool for the agreed time. The payment is directly proportional to: how long you are borrowing it for, how likely will the item wear and tear, how big is the risk that you will damage it while using or even steal it. To counteract those eventualities some of the lenders can demand collateral that will cover some or all of their expenses should you run away with it.
This way you can think about borrowing money as borrowing such tools. Situation becomes much more complicated with the introduction of contracts that cover such exchanges which can mean that a loan is more than just a loan. Another of such complication is the introduction of compound interest. If you lend someone 100 $ for a year and ask that 100$ plus 5$ fee, when you made with it 125$, you are 20$ richer and the lender is 5$ richer. The next year you want to repeat the same exchange but without having to go through the hassle. Instead of having to pay the 5$ after the first year, you use the whole 125$ to make even more the next year. Thus you effectively borrowed 105$ after the first year, owing 105$+5% of that, the next year compounding again and so on.
This mechanism works both ways – when you borrow from someone or when you lend to someone. Unfortunately things become much more complicated with how things work currently than if it was just an exchange between two individuals agreeing on some loan contract.
Money nowadays is usually in two forms – fiat money (debt tender enforced by law) and electronic currency. It bacame this way in the second half of 20th century, when having to guarantee solidity of money by being able to exchange it for appropriate amount of gold, the governments gradually lessened this guarantee to eventually abandon it completely, enforcing the ‘quality’ of money simply through governmental power alone. This allowed the central banks in those countries to manipulate their national currency much more flexibly without having to worry about the amount of rare metals in reserve.
On top of that, by law alone, banks were given the right to create new money from thin air. Banks realized that most of the time money stored with them is sitting idly and people don’t take it out that often (people gradually stopped using gold and used the paper money instead, thus banks could simply issue more of those papers than they had of the gold), so they could get away lending more than then actually had in.
At the beginning of 20th century there were many independent banks and various currencies and banks were running into the risk of people panicking and trying to take out all of their money from them so they would effectively not have enough to pay all out – so called ‘run on bank’. (Most recently this happened in Greece during after the 2008 crisis). When governments and banks realized this panic was not in their interest, they came up with the concept of (private) central banks enforcing a single currency (and thus the central bank effectively regulating how banks operate; private because change of government should not destabilize economy by suddenly devaluing money by printing a ton of new one).
This led to individual banks being effectively monitored by the central semi-independent authority so that they don’t overdo with the lending from thin air and go bankrupt.
Why this complicated detour? Through this power of being able to lend more than they have, the banks are effectively micro-taxating everyone in the society every single time they loan money to anyone. This is because this new money is not being instantly redistributed in the society and through the effect of inflation (everything is on average more and more expensive) (more on this in the chapter below).
This means that given ‘stable’ economy with some inflation in it, people are motivated to take loans to get the ‘freshest’ money and if they can make profit from it, they profit at the cost of microtaxing everyone. The bank does not lose anything (and through so-called multiplicative effect can transform any new deposit made to it into dozens of times more ‘new money’ and also extract the difference between interest rates to it’s own pocket (and it’s shareholders, more below).
Since all of this money is created from thin air and all of it is encumbered by interest, whoever will be repaying the loan needs to extract that extra interest somewhere. Which will again be encumbered by interest and go through the same mechanism of debt created from thin air. You get the picture.
Complication arises from the fact, that wealth or debt can then be inherited. Then add the effect of compound interest and the fact that all money is created as debt encumbered with more interest, and some people are born so rich that just the interest on their wealth keeps them wealthy no matter what and some are so indebted that their whole path through life is almost insurmountable challenge.

### Trickle down carpet

This newly created money has the value of what the current price index is. Once this money starts moving to second owner, third, fourth and so on, usually again through bankins, multiplicative effect, more loans and interests, more people become aware that there is more money available and raise their prices. The further down the chain from the creation of the new money you are, the less benefit from it you have. This becomes especially apparent if you are an employee that has to wait before the employer may or may not decide to raise your pay/wage. The whole time since the prices increased and you got the raise everyone above you in the chains of ownership of this new money was living a little bit at your expense.
Employers are usually not affected as much as employees because they have way more tools to adjust the inputs and outputs and nobody bats an eye (bakery raises price because flour is more expensive, flour is more expensive because fuel for harvesters is more expensive etc more on that in logistic chains). Employee has to either be patient, risk demanding pay rise, or even risk looking for a better paid job.
In an economy with any inflation, making debts is encouraged more than saving money, because repaying a loan becomes cheaper as the value of money dilutes and saving money is thus losing it’s value. Thus savers are encouraged to lend the money away again so it keeps circulating and if the interest paid on it is larger than the inflation, the saver is not losing their wealth.
Economists argue that this is good because people circulating money means that new value is created to repay all of those debts with interest and thus new values are created. More values unfortunately often means more negative externalities like environmental pollution (more on that in shareholder vs stakeholder)

### Logistics chains

With the increase in specialization, goods are often no longer produced and consumed in one location but go through massive chain of inputs and processing, and transportation across the world before it arrives at customers. The more complex the production of such a good is, the larger and more complex and intertwined the chains (more like netwo rks) are. This has several effects.
One of the effects is pressure towards up-scaling of production to take advantage of tools like tax optimization (read tax avoidance), towards mergers, transnational trade and operations in general. This gives such companies operating on global scale (re: globalization) massive advantage over local companies when offering similar products because not only the transnational companies often pay way less taxes than a local producer, have labor costs way lower because they often utilize labor in the poorest countries (who often have also the lowest safety on environmental standards), so they are able to mass produce goods that local producers simple can’t compete on basis of price of comparable quality. This leads to reduction of diversity of market suppliers in target economies and all related problems with it.
Second effect of this increase of complexity of logistic chains is the burden on the environment. Having to transport massive amounts of goods and intermediate products across continents and through countries burdens the environment (and, consequently, us) with pollution and stress.
Third effect of those chains is the increase of interdependence between all those supply structures and corporations. This has become even more apparent during the post-2008 crisis. Countries which has on first glimpse nothing to do with subprime mortgage lending in USA suddenly felt the real impacts on interdependence of economical structures across the world. Large part of this effect was caused by the banks trying to extract value for their shareholders by increasingly risky or unethic ways.
more on the scaling issues viz reach of the civilization pyramid, ethic banking in alternatives section

### Relocalization

An effect that emerged as a counterbalance to this massive globalization of markets is so called relocalization. With some technologies becoming more decentralize-able, in some cases the production chain can be reduced from a global one to a local one. In the cases where this is possible, it is desirable – such as food production not having to harvest unripe produce, less use of preservatives and refinement, keeping the nutritional profile and not having to rely on overproduced food.
Another promising venue is the emergence of 3d printing technologies. Being able to store just the building blocks and transmit the algorithms to build desired products from them over the internet would drastically cut down the need to ferry and transport massive amounts of goods around the planet.

### Information content of work and goods

[TODO] technology turning into consumer goods, ford->gm example, kurvitka
With the gradual shift from hunter-gatherer society, over agricultural, across industrial, to current robotics, automation, possibly AI in future, the goods and services produced contain way more knowledge and information in them. How they are produced, how they are maintainable, how long do they last, what knowledge is needed to try to improve on some of them.
This process is continual and once again directly relates to the specialization depth and scale of society. We simply won’t be able to produce cellphones or have a working space station should the society collapse back a few hundred years due to conflicts, unforeseen catastrophe or otherwise.
The more sophisticated our products are, the more fragile their existence is and the more dependent on the existence of the whole underlying structure allowing this deep specialization.
This gradual move towards more information and knowledge being contained in the goods and services, the more value can those, who master those skills or control resources producing them, extract. This (especially during current globalization) causes massive disparity of wealth between the shrinking group of ultra wealthy and every growing base of this pyramid of the have nots. More in the pyramid

### Maximum reach of civilization pyramid

The constant push (or pull?) towards higher and higher reach of humankind, it’s technological progress and resulting marvels, requires ever growing underlying structure that allow it to reach such depths. This structure takes a form of a pyramid. The lower levels are based on all the cheap labor in third world and low skill labor demand eliminating tech (automation).
This results in several effects.
One of them is the fact, that the amount of levels (layers, distance) between the ones at the very bottom (or simply left behind because they simply don’t have any marketable skills anymore), that it creates social tension (envy, frustration) and on the opposite side elitism, affluence, inability to relate. This can potentially lead to reemergence of class conflict (to which many signs point for example in USA). One of the possible countermeasures being discussed lately is so called ‘basic income’ which is something like a tax on everyone based on some key (probably at least somewhat progressive) and re-distrubution to everyone based on equal amount given to everyone. This is theorized to alleviate some of the anxiety of the lowest layers, especially when the potentially extremely disruptive AI based technologies start to take root.
Second is, that the shaky stability of their tops ten to move the whole pyramid around potentially colliding with other pyramids. This is why most of the wars are initiated by people ‘in charge’.

## Sociology

Interpersonal relations and behaviour of a group of people is not only determined by economical factors but by a host of other ones. Ideologies, religions, art, media, culture, politics.. all of them have some ties to economics but all of them stand of their own as very much capable of such influence over societies. All of those are a form of communication between individuals.

### Sovereignty

Individual. Each of us is more or less able to survive on our own, but each of us needed more than one parent to be born, and will need more than just us alone to produce offspring (living in a world full of just clones of ourselves or like in John Malkovich would be very weird to say the least).
Despite this inherent need for the existence of society for the survival of humankind, humankind would not exist without the individuals. Individuals are what gives society power. Individuals through their specialization and passing of knowledge between generations allowed us to achieve incredible feats that would simply be impossible to do if we existed just as an individual.
Since we are all different – individual – we have also differing views, differing interests, differing capabilities, different fates if you will. When interacting with each other, we aren’t dependent on their views, on their interests, they are their own and for us they are just part of the external inputs we base our own decisions on. This insulation of will is a precious commodity which should be traded off with utmost carefulness. Giving away the power over one’s decisions to someone else is a trade that we always have to be aware is a trade. The price we usually have to pay for having to take it back is often the highest, but also necessary. Otherwise we would live (or even be born) into servitude or even slavery.
One wise person said that the best form of slavery is the one that the slave is not aware of. Remember  that you are always a sovereign if you’re willing to pay the price.
Unfortunately the process of giving away our sovereignty is usually very obscured. Giving away information about ourselves freely to the public means that someone who just collects it without sharing it back reciprocally is basically gaining power over you. Knowledge is power and power is for sale. We are giving it away through shortsightedness -  I want the thing now (just because the society tells me that I need it) and I am willing to put myself in debt for it. We are giving it away through lack of awareness – sharing of private sensitive info unknowingly, skimping over contract agreements, not reading the small print. One can say that sovereignty is nothing more than an illusion.
I would say it’s something that we keep telling ourselves for the sake of comfort. Sovereignty is hardship, discomfort and ultimate responsibility over one’s action.
It is from sovereignty that societal agreement gains any power, and it is only from sovereignty that any power can be given to any government.

### Shareholders and stakeholders

In many forms of incorporated companies there is a possibility to buy part of it and become partial owner of that company. When the company is publicly traded, anyone can buy shares emitted by them and thus also becoming partial owner of such company, although without legal responsibility for that company’s actions. This responsibility usually usually falls on the board of directors that are elected and paid for this responsibility by the shareholders. Shareholders can then either gain return on their investment into such a company or bear loss.
Everyone affected by actions of this company or corporation is then the stakeholder.
For example, if you invest money in a mining corporation and gain dividends from thheir profits, you’re the shareholder. If you live near a mine that company is operating, and your drinking water gets poisoned during a mining accident, you are a stakeholder. You can be of course both, which is for example always the case for enterpreneurs.
The distinction gets a little blurry in the case of a state and government. Since usually the government is not liable for anything between some to all of their actions other than just risking of not being reelected (immunity, indemnity) their stakes are limited but shares are significant. Nonetheless it’s useful to keep in mind the distinction between those two and when analyzing anything ask yourselves the question ‘qui bono’ - who benefits, who has share on this change.  

### Being informed during abundance of information

Issue at hand is choices, control and responsibility in relation to spreading of information and ability to pick out the relevant ones for us and making an informed decision.
In order to make an informed decision (which is the crucial component of free market operation to allocate resources optimally), there needs to be several conditions met:

* we have to have access to all the necessary information
* we need to be able to pick the relevant one without being swamped by nonsense, misinformation or outright lies or manipulations
* we need to understand this information
* we have to want to be informed

Those conditions are contrary to some marketing techniques and business practices. Shady practices are less frequent in richer societies where there is real competition between companies fighting over customer’s business. On the other hand, being flooded with choices also often leads to indecision.
Pressures mentioned earlier (viz globalization) that lead to mergers and lower diversity and suppliers means that there is actually less choice, entry of small companies as competition is the more complicated the more bureaucratic and administrative hoops they need to jump through (those become negligible as the company scales up thus they favor big business over small ones).
Even then, when you decide that you want to go with a different supplier, you might discover that in order to go with that choice, you’d have to go out of your way to make that choice. You have to make conscious decision if the extra time, effort or resources you spend going through with that alternative choice is worth it.
This is not only relevant to shopping and making choices then. The same problem involves contemporary media. Old media channels are gradually taken over by the so called ‘new media’ - internet and especially social media. Becoming a source of information is become so cheap and easy, that we are becoming more and more flooded from all directions by this ballast (unnecessary or trivial information, distractions).
Some of this flood can be misleading, misinformation or outright lies and manipulation. Having and practicing the skill of critical thinking can be sometimes cost prohibitive. It’s becoming gradually more difficult to trust someone. Thankfully over time, some natural authorities that have integrity and consistency emerge and people flock to listen to them like moths to a source of light.
This role of ballast curator is a thankless one but critically needed during such a flood.

### Delusion of reality perception

More often than not though, we falsely believe that we have our proverbial ducks in a row and we believe that we ‘get it’. We’re woke. We’re not. Do not believe anyone at face value, especially if that face is looking at you from the mirror.
We as humans are built for comfort and optimization of resource expenditure. We tend to quickly skim over what is being presented, make a snap judgment, commit to it and then stick to that decision especially when others disagree with us, because it must be them who is wrong.
Our skill sets are limited, the knowledge we acquired during our studies is outdated, we do not spend the extra time double checking the facts, look for alternative independent sources, we are no journalists.
Unfortunately most of the contemporary producers of information think they are. They are not. Most unfortunately though, in an attempt to compete with this new media storm, the ‘old guard’ are ceasing to be journalists too. Most of the information you are reading is regurgitated, copied, pasted and presented as genuine. Often there is one source, occasionally it turns out, the information is completely made up, unverified.
Even worse, various interest groups realized this and became masters at manipulating those flows of information to inject their agenda. Information that we freely divulge to the companies running those social networks is utilized as variables when generating flow specially tailored to our interests and even politicians are starting to take advantage of those technologies.
Being able to specially custom tailor and deliver message to each individual voter that will resonate with them and make them more likely to vote for a candidate that they would otherwise not voted for is turning elections into pure marketing campaign.
Professionally done campaigns then lull us into possibly voting against our own interest and there is usually little we can do about rectifying that mistake. Even worst though is, that this manipulation does not need to end by the time the election campaign ends, but can be permanent, especially if the politicians become entangled with the sources of information that we are supposed to trust to be the guard dogs of democracy – unbiased, daring, dutiful and unflinching journalists.
What results, is that we either do not learn from our past mistakes and keep trusting new and better formulated promises of a candidate that will finally bring order, prosperity, end corruption etc. As always. There is little that we can do post election to affect the course of our environment the higher up it is in the pyramid.
Certain democracies allow their citizens to directly affect a particular vote through referendums, but those tend to be costly in order to make them less prone to being manipulated. _More on that in semi-direct hybrid voting_

### Overton window  - move this into subnotes or references

There is a concept in sociology called Overton window – what is considered acceptable in a given society and culture at a given time.
(what is acceptable / (culture * time) )

### Resolution of conflicts

(lack of communication - scale from peaceful discussion to war)

### Limited laws, judicary interpretation

(constant growth of complexity, 'code rot (in law)')

## Math

### Gravity of power and money

Assumption: power tends to aggregate and accumulate like mass and exert force like gravity, due the fact that most of people want to have various amounts of it, having zero power means one is either a slave, prisoner or to a certain degree a hermit
Assumption: Being exposed to the power (often facilitated through the tool of money) exerts a force upon integrity of a person (or a society – see pyramid) over time.
Assumption: People whose personalities are more structurally sound, have more integrity can withstand it for longer time.
Assumption: People wielding power can add it together to overcome the power of someone else
Postulate: people with enough integrity can overcome the force and get rid of some of the power and this the force acting upon them
Postulate: this force is commonly known as corruption
Postulate: this force can act through intermediaries like connections and compromise
Postulate: this force is corrosive and chips away at the personal integrity over time, so the longer it acts, the less capable of withstanding it any individual is
Postulate: this force is innate – always exists and always will – it stems from the grouping of people and their specialization, same as money

### Vector of dissatisfaction

In math, there is a construct called vector. To roughly simplify it, it’s a set of values that express a direction and at the same time length of such vector. For example if we assume a two dimensional (2D) plane with two perpendicular axes x where x grows to the right and y to the top, a vector of [x=1,y=0] will be pointing to the right and have a length of one. If we were to add one dimension Z (growing to the ‘back’) for a 3D space, a vector of [1,1,1] would be pointing to the, ‘top-right-back’ and have a length of roughly 1,7. We could add as many dimensions as we want and we would still be mathematically completely fine, only the illustrations would not as easy. Explaining how the length is calculated out of scope of this publication but can be easily found in math publications or through googling.
For now, we just need to know what a vector is, that it can have more dimensions and that calculating the length is trivial.
Assumptions: Now, let’s assume that every individual has different values, different perception of how reality is around him and the how he would want it to be. If we consider each aspect of this perception, we could express this difference as a theoretical number. Zero – that aspect agrees completely with how we want it to be, large number – this aspect is very unbearable and we would do anything to change it.
Combination of those aspects could be put into a vector. Size of this vector would then express our overall dissatisfaction with our perception of reality.
Postulates: The closer a message vector (political statement for example) will be to our perception, the more receptive we would be to the rest of the messages coming from the same source, the closer will be the largest values and the lower will be the difference to our other values with our higher numbers having more weight. In other words, we are more receptive to someone who agrees with us on things that annoy us the most and we are forgiving on differences we think are currently fine. Up to the point this difference is higher than that of our highest annoyance value, by which point it would become our higest annoyance.
Postulate: those ‘dissatisfaction vectors’ could be averaged across population and give us a single vector that will then determine in which direction the society wants to move. If a government will promise to move in that direction, the society will be overall more satisfied.
Postulate: if we consider a range of ‘tolerable thresholds’ of every of those values – for example if we consider one of the dimensions  - how many days after conception a pregnancy can be terminated – we can express the range an individual will tolerate. The ranges between individuals can overlap. The more overall rate of overlap, the more moderate will that society be relative to the Overton window.
Postulate: the narrower those tolerance subsections, the more precise the political messaging can be and the more receptive people will be the larger one of their values will be. Simply – a person with a dissatisfaction vector of 0,0,0.. will be immune to political messaging. Someone with a vector of 10,10,10 getting promissed 10,10,10 will become a fanatic.
Postulate: the less overlap between those tolerance subsections, the more such a society will be to internal tensions and divisive/polarizing politics, swinging wildly between extremes.
Postulate: tracking evolution of the dynamics of those tolerance subsections can predict how a society will behave in near future with same chaotic inaccuracy as predicting weather – the more precise we can measure the dissatisfaction vectors, the more reliably we would be able to predict the behaviour of such society.
Postulate: the higher the number of small dissatisfaction vectors in society the more stable and resilient against malicious messaging manipulation by things like media such a society will be. This means that direct democracy methods are dangerous to be used by societies who are wildly polarized (the average dissatisfaction can be small, but the overlap can be small across the whole, or there can be overlapping groups but not overlapping with other groups (red vs blue. Democrats vs republicans etc)). Direct democracy will work the better the more moderate and overlapping those DV’s tolerance ranges will be.
Postulate: reducing the overall aggregate percieved dissatifaction is the purpose of politics, reducing the difference between the aggregate perceived dissatifaction and aggregate real dissatifaction is the purpose of education.

### Dynamic stability

No democratic system in the world has been so stable and rigid, that there has been no change in it’s governing (ruling) political party of a coalition. People age, change opinions, die, new people enter the party, outside conditions and environment changes etc.
What happens in most ‘stable’ democracies through is a cycle between focus on an individual (freedom, competition, individual wealth, elitism) and the collective (control, cooperation, equality, public good, populism). The amplitude of the swing between both ‘opposites’ is the measure of stability of such a system (lower amplitude, more stable). This is what we call dynamic stability – there are changes in the system, but it oscillates around an imaginary middle.
This oscillation does happen on various time scales. The most apparent is in the election cycle. Other oscillations do not necessarily happen in a regular time intervals but rather based on larger scale disruptive changes like revolution, war, catastrophe, cultural (abolishment of slavery), shift in demographics (Lebanon), or environmental change.
Ideal state is not one, where there are no oscillations, but such, where the swings manage to satisfy (reduce) the vectors of dissatisfaction in aggregate.

### Saddle curve of meddling

Assumption: the price of covering the lowest levels of Maslow’s hierarchy of needs is pretty much constant, but the higher we go on the pyramid, the more can those costs vary on person to person basis.
Assumption: postulated above, being exposed to power (through money) exhibits corrupting force on the character the longer the exposure is and the higher amount of power.
Resulting fact: the percentage of income spent on covering the lowest levels of the hierarchy is the higher the lower overall income is. This is one of the reasons any sort of ‘safety nets’ exist in a given country. Person unable to cover the expenses on the very basic stuff is inevitably unhinged and due to existential needs is willing to go to the extremes to fulfill those needs. This can range from theft, robbery to acts forgoing own existence like suicide (self immolation with a message), ‘paid’ terrorism (family of a suicide bomber gets paid by the interested party after the fact) etc.
Postulate: Any state trying to optimize the outcome of those pressures should focus it’s energy (and control or meddling) into both of the extremes of the scale between absolute poverty and maximum wealth. The middle should be left alone as much as possible.
This is for two reasons: one – if the base (the lowest income section – the lowest level -  of the civilization pyramid) is compromised, all of the above levels are endangered. If the people on this level honestly try to make ends meet, and are unable to, the whole pyramid becomes unstable.
Leaving the middle section alone means that the structural integrity of the whole pyramid will remain sound. They are like the steel reinforcement in concrete. Without it, any stronger shaking will once again de-stabilise the whole structure.
The second reason: the very top of the pyramid has the power and ability to (metaphorically speaking) alter the recipe how the steel and concrete is made so it’s cheaper to build higher but the whole structure can once again become compromised. Fortunately for them, they have private jets so they can fly to a neighboring pyramid and settle there instead. Less fortunately for everyone in the previous pyramid without such an access.

### Solidarity rating

Fact: ability to extract taxes depends on many factors. Overall amount of taxation, quality of services provided for them (how efficiently the state spends them), the demand for those services.
Assumption: if the state spends money extracted from taxes contracting private sector to supply services or goods, it potentially negatively affects the market. This has to do with moral hazard (companies instead on focusing on competitiveness with other companies in the (global) market, focus on landing a contract from the government.
Assumption: people deciding on where to spend (or store) their money money usually don’t have access to hard data about how the subject they are about to pay their money to acts in terms of ethics.
Does the company pay their fair share of taxes or are they avoiding them through ‘tax optimization’ or tax evasion, ‘paying’ taxes by being registered in one of the tax haven/paradises? This is further complicated by the level of interconnections between corporations and massive amount of levels between who controls them and what is facing the public (brand).
Do you know, if your bank is offering loans through intermediaries or subsidiaries to some African warlord to buy guns before you decide to store your money with them and thus make them a lot richer by effects like multiplication effect or ability to extract wealth from thin air by means of law alone.
Do you know if your clothing company is using slave child workers in Bangladesh to have the competitive edge on global market? 
How is your food sourced?
Do you even care?
Proposition: introduce a ‘solidarity rating’ - an opt-in program of auditing and control of a company that will track how much wealth they are trying to extract and siphon from the people or if it’s keeping the wealth where it was produced by paying taxes or reinvesting it locally. This measure can then be used not only by individuals deciding on where to spend their money but also by the state on who can attempt to get any of the contracts the state will be offering. The technical implementation of such a system of tracking and auditing (including full transparency in terms of ownership relations) are outside of the scope of this book, but might be elaborated upon in the future.

### Fractal law

Laws – the code of the framework we’re operating in undergone significant growth since the inception of this concept - ‘eye for an eye’ in ancient Mesopotamia. Today’s code of law is so complex and detailed, trying to cover every aspect of ever changing reality, that nobody is able to understand it all and even the experts work with just a tiny fraction of them.
Assumption: This process of eternal growth in complexity leads to them containing many inconsistencies, flaws and loopholes, that are often inserted there intentionally, so they can be later exploited. And since willingness to fixing of such loopholes is usually very lukewarm (due to ‘lobbying’), they tend to exist for a long time or reemerge in a slightly different form like in a twisted version of ‘whack-a-mole’.
In history, Christianity introduced a concept of ‘ten commandments’. Simple rule set of don’t-s that everyone can remember and understand and left all other intricacies of dispute settlement to the judges.
Proposal: Introduce a hard limit on how large set level of ‘commandments’ can be and have the subsequent details limited in similar fashion, forming a hierarchy that can be incrementally understood from the top level down as needed by a profession. For example a commoner would need to know only the top level to be well off most of the time. Second level of detail on when they will want to interact with others in a more formal way like contracts and property trades, third level when dealing with lawyers, courts and bureaucrats.
Proposal: This hierarchy should be self-similar
(10 commandments, recursion)

## Politics and tech

Politics relates to everything relating to ‘public life’ ranging from culture, economics, sociology, religion and other fields. It affects many aspects of our every day life, because governments and laws decide on the limits of what we can do (or be punished for going outside of those limits) and I can go as far as to say, it prescribes a framework of how we interact with each other in the public space.

### Labels and axes

For a long time (from the perspective of a single human life) the terms used to talk about different groupings in politics have been hammered in people’s heads. To this day, people label political parties by putting them somewhere on a single axis between ‘left’ and ‘right’.  This is very reductionist, simplistic, and misleading. Let me explain why I believe it to be so.
First and foremost, the terms ‘left’ and ‘right’ are a compound of various characteristics, that most parties don’t fit very well and thus political commentators and analysts have to try and transform the inherent complexities and nuances to this scale.
This in turn confuses voters that then often vote against their own interests (and in fact moves the dissatisfaction vector from where it would be, without this deformation, to where it’s perceived to be).
The actual characteristics are a set of one-dimensional scales, of which we can list a few (most of those axes are independent of each other but some can correlate:

1. collective – individual
1. authority – liberty
1. conservative – progressive
1. isolationist – cosmopolitan
1. elitist – populist

Important bit of info is that those scales are value-independent. There is no ‘good’ or ‘bad’ - it is always relative to the viewpoint of the observer (and in turn to the dissatisfaction vector).
Collective – individual axis relates to the history and religion and in the whole society is almost always a mix of both. There will always be more people whose mindset is geared more towards thinking about groups and collectives, be at home with terms like welfare, public good, class, solidarity, equality (of outcome), collaboration, collective responsibility. But at the same time, in the same society, there will be people comfortable with terms like independence, hard work, individualism, individual responsibility etc.
This, of course, is true only if the opposite opinion is not suppressed like it is in authoritarian regimes.
Authority – liberty axis is independent from the collective-individual, but often people holding collective opinions tend so towards authoritarianism and individual leaning towards liberty. Exemptions (authority+individual) is usually an extreme of dictatorship (quite frequent on small scale, harder to pull off on larger scale where the individual turns into collective). Liberty+collective on the other hand is anarchism (anarcho-communism). As you can see, those exemptions are usually not so stable and tend to decay into one of the other more correlating forms.
Authority and collective examples are all forms of fascism and communism (or state enforced socialism for that matter). Liberty and individualism examples are free market capitalism.
(Regressive) - Conservative – progressive axis is probably the most misunderstood axis. Those terms used as labels are the least descriptive in terms of useful information and are completely dependent on other characteristics and thus can be determined from them. Conservative policies simply want to keep things as they are (status quo). You can think of conservative as being in the middle, because this scale does go even beyond that and some policies can be regressive (trying to bring things to the state they were in the past). Last extreme are the progressive policies which want to move things to a state that they weren’t before. Often relates to some time period.
Isolationist – cosmopolitan scale is a bit more complex. We can also think of it in terms of closed – open, inert – intervening, focused inside – focused outside, local-global etc. All positions on this scale can have both negative and positive consequences affecting many other parts of the world and history, and as such are complex usually need to be analyzed from historical perspective.
Elitist – populist – this scale determines the scope of focus of this position. Elitist position focuses on a selected group of the population and argues using terms like expertise, education, science and appeals to higher civilization principles and reason. Populist position on the other hand, tries to appeal to the widest possible electorate with messages that needs to be understandable by many, thus often appealing on lower principles, emotions, common sense, tradition and folklore.
Political parties that position themselves on those axes (the width of such position also varies) then offer answers to the dissatisfaction vector (each of those scales is part of the vector as well).
Media relying on the left-right dichotomy is damaging this adaptation and evaluation process by both trying to convert all of those axes into one both in the direction of media message from the parties to the voters but also causes voters to try to squish all those axes into one in their own mind.
This not only leads to the confusion, difficulty to ‘read’ what position does the party represent across multiple topics (for example isolationist-cosmopolitan axis very well describes the foreign relations ministry, how does the left-right?), but also amplifies the dissatisfaction vector’s lengths, because people are unable to match the offer from the political parties with their ideas (perception) of the reality and thus often miss (not bearing in mind other factors of course).

### Law as code

We can draw many similarities between the world of information technology and real world. Patterns existing in one world can often be found in the other.
For example – computer code is in definition just a sequence of 1’s and 0’s and the hardware that executes this code knows how to interpret it and to decipher what are instructions and what is data. There are different levels of abstraction above this with thousands of systems and subsystems interacting with each other with varying degrees of resiliency towards errors or even malicious code.
To be able to interact with those systems, programming languages were invented to translate human readable and understandable concepts into the code of 1’s and 0’s. Those languages usually contain features like variables to hold data, operations on those data, transformations of inputs into outputs and some even higher level concepts like objects. They are linked all throughout with references. To be able to use those languages, you need to understand higher level concepts like algorithms and the syntax expressing those algorithms.
Now compare this situation to a set of laws. The comparison I’m about to draw is certainly not 1:1, but bear with me.
Laws are a system of code, they contain a definition of terms that they are using (variables), they are linked through a set of references, you need to understand the language they are written in, they are used to transform higher level concepts into a set of rules that transform inputs (human interactions that are governed by those laws) into output (regulating behavior or corrective actions to keep the system working).
Now consider this: our level of understanding of the concepts used in information technology is on a much higher level than that of laws and real world. Developpers trying to keep their code bases in check use highly sophisticated methods and patterns to keep the code working well, be maintainable and scaleable.
Why can’t we use those tools to help us maintain our laws? Automatically parse the current laws and find bugs, loopholes, optimize the performance, measure the performance, decouple overly complicated laws to be understandable by more people than just lawyers.
Relates to the fractal law.

### Rights and their enforcement

Ever since the inception of the concept of rights and contracts, the issue always was, that every right and contract comes with having to enforce it. Without enforcement of rights and contracts, nobody is punished for not respecting the rights and people not willing to follow preagreed rules by both parties can quickly get into conflict or even war.
This ties to several of previous chapters – sovereignity, cycle of war and even the vector of dissatisfaction.
People tend to forget that with each right comes enforcement of this right. For without enforcement even a concept like inalienable human rights is just an abstract notion.
If we consider a set of rights and possibly also set of rules and laws we obey to be the thin civilization around the mass of animal instinct, dismantling, corrupting and otherwise breaking or destroying of this layer is what eventually leads to conflict or war.
No matter what ideology or arrangement, there will always be extreme ends of a spectrum, where on one end are ‘straight shooters’ and the other will be occupied by ‘crooks’. Even if you and your friends decide to live like hippies, what will you do once a greedy neighbor comes and decides to rule over you? We’re coming full circle to the specialization depth and Maslow’s pyramid of needs.

### State meddling, privacy

In the recent years, internet based technologies bring up a host of disruptive trends in many areas of people’s lives. One of them is the concept of privacy. There are adults for whom internet existed their whole life. A slightly younger generation is growing up with the ubiquitous presence of social media everywhere.
There, almost nobody is concerned with privacy. The companies who own the currently largest platforms make their money by collecting, processing and reselling user’s private data.
Even though, most concerns about privacy were in relation to the power of the state, those large corporations seem to be the main perpetrators of the privacy abuses and breaches.
Preemptive taxation buffer
According to one economical theory, people are the more willing to pay taxes the lower the taxes are. Unfortunately this relation is not relevant for at least two reasons.
One – It’s not tied to how much money you extract from people – you gain overall more tax money on higher level of taxation that people imagine.
Second – people will pay taxes if they want or not simply by not knowing they are actually paying taxes – we can call this ‘obfuscation of tax burden’.
This way, the overall taxation of people based on their overall income can be often pretty skewed. This is caused by several facts.

* expenses for basic needs are actually higher on the lowest levels of income – low quality goods break or fall apart more often forcing the people to invest into them again (clothing, boots, ‘white electronics’), spend more time on handling those basic needs (thus not being able to invest that time into making more money)
* in terms of enterpreneurship, most markets have barriers to entry and administrative or bureaucratic burden, that takes away once again time and resources, that can be much easier offset by a larger company.. this contributes to permanent pressure towards mergers and fusions that lead to market verticalization (few large entities instead of a host of smaller ones), until a crisis strikes (economic cycles, recessions) and even more pressure causes some companies to fold and the rest to fuse or merge (instead of trying to out-innovate)
* high income brackets have many more tools available at their disposal to hide and obfuscate their true wealth (tax heavens, tax ‘optimizations’ etc) and thus are in fact paying lower taxes than the ‘middle class’ income bracket

Unbeknownst to most people, this fact alongside usually poor performance of the government and bureaucrats is adding to the unwillingness to pay taxes if possible. Worse ability to extract taxes from those who have extra resources leads the governments to extract them from easier targets, that usually don’t have any means to avoid this. For example government can increase indirect taxes that are tied to consumption or value added taxes which once again in turn affect disproportionally the lower and lowest income and wealth brackets.
Now consider this concept: opt-in preemptive taxation buffer 
An opt-in system, where a person (or company) voluntarily decides to be transparent about their wealth and declare it, (or lose option to participate in this system), and gets assigned a ‘taxation buffer’ that they can then fill up as needed as the year progresses.
Person participating in this system would then be eligible to have percentage discount of all interactions with the state. This includes everything ranging from emergency services, road tolls, any interaction with the government that involves government resources etc.
Payments for those services would be taken from the already filled buffer after the discount (no buffer, no discount), and any extra money owed will be owed to the state. Not paying this money in due date will be considered a tax evasion and punished according to the law.
Any remaining money in the buffer at the year would then be divided into two groups. One being mandatory minimum for state survival (army, emergency services etc) and the rest would be up to the person to decide how to be spent on non-mandatory expenses (social transfers, non-critical healthcare, welfare safety nets, public goods (environmental protection etc))
The money owed to the state could potentially be forgiven in matter similar to tax forgiveness.

### Semi-direct hybrid voting

As mentioned before, all source of power in a society is derived from the sovereignty of an individual that is then exchanged for goods and services. It means that people don’t have to rely only on themselves to be handle all the necessities and allows them to focus on what they’re specialized in.
As different forms of governance evolved over the time with increasing scale of population that needed to be governed, all of those forms exhibited similar characteristics. Those were always hierarchical in any large enough grouping of people. Efficiency of a hierarchically structured society where the hierarchy itself is natural and based on merit (people best in their field of expertise are in their fields of expertise) tends to be higher than those, where the hierarchy is artificial (nepotism, oligarchies). Longevity of such arrangement is then affected by how much the people living in it agree with how it’s layed out.
This aspect of longevity or stability applies not only to hierarchies that are artificial, but also unnecessarily vertical (the pyramid does not have large enough base to keep the higher layers stable) or the lowest levels don’t have to exert that much revolutionary shakes to bring it down.
Stable pyramid needs to be able to support N+1 floor by strong enough floor N.
In democratic societies of large enough scale the systems of (re)generating parts of this vertical hierarchy developed over time. The system mostly used in the ‘first world’ is usually a democratic republic with representative governance, which is characterized by regular elections that allow people to choose other people (or themselves) to be representing the will of a group of people of that society.
This is usually achieved by having several political parties that aim to capture the most wide and well defined group of dissatisfaction vectors with their messages, promises and actions.
The system of representative democracy was invented during the time where overall conditions and restrictions did not allow any better way to allow sovereign citizens to express their aggregate will when answering the questions that are asked in the public governance domain.
We no longer have to use horses to travel between cities to deliver our messages written on paper. Ever since the invention of the internet, the way we communicate was revolutionized. The costs of such a communication are miniscule and we can communicate pretty much instantly all over the world.
Why not use the power of such a tool to also iterate and hopefully improve on the system of representative governance itself?
The main reason for this pursuit is the possibility to marry the direct democracy and representative democracy properties into one system, that would take better from both worlds.
Some time ago I and a couple of my friends came up with an idea for such a system.
There are several characteristics:

* On the scale between security and convenience it has to be as secure as possible given the convenience of people being able to express their will in any vote they are affected by given the context they live in (and depending what parts of other compound structures and organizations those levels are part of). For example person A would be able to cast their vote in any vote regarding their district, county, country/nation, but not in county vote of a neighboring county or  nation.
* It has to be cheap enough to maintain to allow this frequent voting by all eligible people – like perpetual mini-referendums with the difference of not having to each time print a new set of ballots, open voting places and count the paper ballots there
* It has to increase the cost for someone trying to manipulate such votes, and try to be as resilient against such attempts in general
* People need to be able to individually change the representative they’ve chosen at any point by withdrawing their vote weight from the representative’s voting weight – this means that even people with a couple of friends that value their opinions could vote with a weight higher than 1 and small parties will have their say and people won’t be forced to vote for large parties
* Those properties would also mean, that the concept of ‘coalition government’ and opposition would be pretty much obsolete, because every vote would be a mini-referendum, and the will of the electorate will not be decided by the agreement behind closed doors between a handful of representatives
* This would also mean, that the more the vote will be technical or bureaucratic, the more the system will behave like the current representative one, where as the more the topic would be a controversial one, the vote would be more affected by the votes of lower weights (similar to minority shareholders)
* This means that whoever would want to manipulate such a vote would have to not only affect the population by media campaigns or bribery like in the case of referendums, but also by lobbying of elected representatives with very vague guarantees that the investment will actually insure the desired outcome – it will be more expensive across the board
* Since referendums tend to be more conservative, this would naturally push towards trying to come up with solutions and proposals, that have broader consensus, so that they will actually go through
* People who are not interested in following politics in detail will thus have the option to stick with the ‘old’ system of casting their vote for their representative, on the other hand, people who will want to be more involved short of running for office themselves will have the option of casting their vote with the weight of 1 in any vote that will be held and they eligible to vote in it
* It’s feasible to imagine variants of this system in trying to minimize potential negative consequence, like for example allowing people to only cast vote ‘against’ and never for, thus turning the ‘minireferendums’ into a pure brake (like a massive version of the senate) that would make it harder to pass laws against the will of the people but at the same time disallow some powerful entity in hijacking this mechanism through manipulation in media to pass something potentially damaging
* with such modifications in minds, the possibility of ‘withdrawing’ weight from the representative should be kept, possibility of transfering it to someone else would have to be eliminated if we want to prevent ‘pro vote’ manipulation of the masses

### Subsidiarity based introduction of laws and changes

Assumptions: In a representative democracy, the government is usually the source of new legislature, which they formulate and refine at their respective ministry. The next step is usually an iterative cycle of voting and optionally getting the law returned from the senate (if the type of government structure uses those two bodies).
The methodologies used to formulate and come up with the ideas is limited to the imagination of the representatives (elected as ministers) and as such is usually very shortsighted and formulaic.
If you think about the process of lawmaking as writing computer code, it’s very bloated, full of bugs, loopholes and legacy code with the invention limited to procedural and syntactical domains.
There are no senior software architects, no awareness of code rot and the need for constant refactoring.
What’s even worse, changes to the whole system are done by majority rule instead of merit, facts and reason. Legislative commissions and other tools are used to keep the syntax correct but the overall architecture, elegance, quality and especially maintainability are left in the dust. And we’re not even speaking about the ‘common folk’ who are supposed to not only understand this ever increasing bloated mess, but also to follow the rules or be faced with a punishment.
If you add to that the ability of interest groups to affect those laws and smuggle in loopholes and rules that benefit them over some other groups, you’re in for a trouble.
In countries, where you don’t even have general referendums (which is the majority of democratic countries), the people who manage to spot those loopholes have no way of initiating a vote on a change that would fix it and instead have to wait for the next election cycle in hope that someone will pick this particularity and run with it. Good luck with that.
Only way to make either such surgical changes or initiate a systematic refactoring done by someone who actually understands how to do it properly, is to get enough money and try to lobby some representative of a currently ruling coalition or party to introduce this change and hope that they will vote on it favorably.
Corrupting potential of such approach is apparent.
Proposal: introduce a layered system of direct law or policy initiative, that would allow anyone with given patience, resources and arguments formulate a proposal, get enough support in the respective layer based on subsidiarity (scope is based on how many stakeholders does it involve and thus have to have a say in the matter).
This process would always have to start at the lowest level and iteratively require all affected parties (aforementioned stakeholders) to have a say. For example: a proposal affecting a ctiy will first have to be agreed upon in the district where the proponent is from, (and people of the district will be informed about this proposal and get to vote on it), then it will have to gather support on the next higher city area  (optionally), and then on the whole city level.
Each level will pool those votes based on which ones gathered most support and interest and also make sure that it’s not just a spam of the same proposal over and over. The particular system of implementation how to handle the spam of busybodies and other measures to have the system actually useful and not annoying is up for discussion.
The frequency of the votes on the smallest levels will be highest, and with increasing scope of affected stakeholders the frequency will go down (for example voting on district level could be weekly, city monthly and state yearly).
There could also be a possibility to merge proposals into multi-proposals so that the voter will pick and weight multiple variants of a proposal to not just limit the voting to yes/no over and over. Weighted multiple choice voting is often more comprehensive and offers more accurate representation of the sovereign’s will.
This way, people with extra time on their hands could dig through the proposals and argue about them online so that when it comes to the actual vote, people will have more than enough information to base their voting decision on, instead of being pressured under time duress.
Similar methodology could be applied to votes that attempt to isolate themselves from larger bodies like in the case of a secession or declaration of independence. Such a process usually involves all higher levels of stakeholder scope so this process could at least allow time to discuss the issues openly and hopefully come to an agreement and peaceful resolution instead of resorting to violence and enforcement. Recent examples of Brexit or Catalan independence vote come to mind.

## Alternatives

Above suggested proposals are one among many suggestions on how (atleast in theory) things could be done differently in hopes of improving them. Many of such alternative approaches actually already exist but usually don’t manage to steal any spotlights and either go unnoticed or are outside of the mainstream focus. Below I would like to shout out to a few of them in hopes of broadening your own perception of what is possible and how people go about certain things.
Every time one tries to dive into ‘alternatives’, one has to be aware of some properties of those fringes. More often than not, proposals, claims or inventions are exaggerations, false promises, hoax, vaporware, wishful thinking, an outright lie or even a fraud.

### Technological solutions

Interesting aspect relating to the logistical chains and relocalization mentioned above are so called ‘island systems’. This refers to systems, that are capable of operating completely independently of outside inputs other than the ones already present in the natural environment.
For example – the so called ‘earthships’ - buildings built using mostly waste material and soil, that utilize systems like: 

* rainwater collection, water treatment by filtration using plant root systems and sand to turn it into drinkable water and store it in tanks for use during dry spells
* sewage and wastewater treatment to be usable as fertilizer for food producing plants
* heat recuperation and management through system of ventilation and storage inside soil

Those combined together in the concept of earthships allow habitants of such earthships to live quite comfortably and self-sustainably without the need of much outside inputs of resources.
Another example of such technological solution that improves sanitation and drinkable water availability in extreme conditions is the rollable barrel (ref) or drinking straw (ref)
Another is the possibility to de-centralize production and storage of electrical power without the need for massive supply chain and production facilities for tech like solar panels (at the moment.. there are attempts to make energy producing tech much more simple in those requirements but we aren’t there yet).
Example of renewable production of power could be from tethered high flying kites (ref), tidal or wave powerplants (ref) or temparature differential wind towers (ref)
Storage of such energy could be in hydraulicaly raising blocks of stone (ref) , inside heavy magnetically suspended flywheels in vacuum (ref) or using pressure differential of compressed air (ref).

### Peer to peer economy

Uber, airbnb, couchsurfing

### Alternative forms of ownership

Coops

### Alternative forms of banking

Ethical banking, microloans